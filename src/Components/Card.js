import user from "../user.png"
import { useState } from "react"
import firebase from "firebase"
import { ConfirmDelete } from "./ConfirmDelete"

export const Card = ({ props, currentUser, getCard }) => {
  const [isEdit, setIsEdit] = useState(false)
  const [card_info, setCardInfo] = useState(props.data)
  const [isDelete, setIsDelete] = useState(false)
  
  const checkPermisstion = () => {
    if (card_info.author == currentUser) {
      setIsEdit(true)
    }
  }

  const onChange = (e) => {
    setCardInfo({ ...card_info, [e.target.id]: e.target.value })
  }

  const setDefault = () => {
    setCardInfo(props.data)
    setIsEdit(false)
  }

  const updateCard = () => {
    var card = firebase.firestore().collection("card").doc(props.id)
    return card
      .update(card_info)
      .then(() => {
        alert("Document successfully updated!")
        getCard()
        setIsEdit(false)
      })
      .catch((error) => {
        alert("Error updating document: ", error)
      })
  }

  const deleteCard = (arg) => {
    if(arg == false){
      setIsDelete(false)
      return
    }
    
    firebase
      .firestore()
      .collection("card")
      .doc(props.id)
      .delete()
      .then(() => {
        alert("Document successfully deleted!")
        getCard()
      })
      .catch((error) => {
        alert("Error removing document: ", error)
      })

      setIsDelete(false)
  }

  return (
    <div className="w-full p-4 overflow-hidden border rounded-md shadow-sm card">
      {isDelete ? <ConfirmDelete confirmDelete={(arg) => deleteCard(arg)}/> : null}
      
      <div className="flex flex-wrap justify-between font-bold text-blue-400 uppercase">
        {isEdit ? (
          <select
            className="p-2 font-bold border rounded-md"
            value={card_info.header}
            id="header"
            onChange={(e) => onChange(e)}
          >
            <option id="react" value="react">
              React
            </option>
            <option id="angular" value="angular">
              Angular
            </option>
            <option id="vue" value="vue">
              Vue
            </option>
            <option id="html" value="html">
              Html
            </option>
            <option id="css" value="css">
              Css
            </option>
          </select>
        ) : (
          <div className="font-bold text-blue-400 uppercase">
            {props.data.header}
          </div>
        )}
        {isEdit ? (
          <div className="flex">
            <select
              className="w-full p-2 border rounded-md"
              value={card_info.status}
              id="status"
              onChange={(e) => onChange(e)}
            >
              <option id="open" value="open">
                Open
              </option>
              <option id="close" value="closed">
                Closed
              </option>
            </select>
          </div>
        ) : (
          <div className="ml-auto" onClick={() => checkPermisstion()}>
            <span
              className={`inline-flex w-2 h-2 ${
                props.data.status == "open" ? "bg-blue-400" : "bg-red-400"
              } rounded-full opacity-100 animate-ping`}
            ></span>
          </div>
        )}
      </div>

      {/* card content */}
      <div className="flex py-6">
        {isEdit ? (
          <textarea
            className="w-full p-2 border rounded-md"
            value={card_info.content}
            id="content"
            onChange={(e) => onChange(e)}
            rows={5}
          ></textarea>
        ) : (
          <p>{props.data.content}</p>
        )}
      </div>

      {/* name */}
      <div className="flex flex-row py-3">
        <div className="pr-8">
          <img src={user} className="w-10 h-10" />
        </div>
        <div>
          {isEdit ? (
            <input
              className="w-full p-1 border rounded-md"
              value={card_info.name}
              id="name"
              onChange={(e) => onChange(e)}
            ></input>
          ) : (
            <p>{props.data.name}</p>
          )}
        </div>
      </div>
      {isEdit ? (
        <div className="flex flex-col space-y-2">
          <div className="flex flex-row space-x-2">
            <button
              className="w-full font-bold text-white uppercase bg-red-400 rounded-md"
              onClick={() => setIsDelete(true)}
            >
              Delete
            </button>
            <button
              className="w-full font-bold text-white uppercase bg-blue-400 rounded-md"
              onClick={() => updateCard()}
            >
              Save
            </button>
          </div>
          <button
            className="w-full font-bold uppercase bg-gray-100 rounded-md"
            onClick={() => setDefault()}
          >
            Cancel
          </button>
        </div>
      ) : null}
    </div>
  )
}
