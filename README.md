# Sertis-mini-blog

This project was created by `react` and `tailwind`(css framework) and backend `firebase-firestore` 

## Available Scripts

In the project directory, you need to run `npm install` first, After that you can run:

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
