import { useState } from "react"
import firebase from "firebase"

export const Login = ({ userInfo }) => {
  const googleLogin = () => {
    var provider = new firebase.auth.GoogleAuthProvider()
    firebase
      .auth()
      .signInWithPopup(provider)
      .then((result) => {
        var credential = result.credential
        var token = credential.accessToken
        localStorage.setItem('x-',token)
        var user = result.user
        userInfo(user.email)
      })
      .catch((error) => {
        var credential = error.credential
        alert("Google Login unsuccessful : ", credential)
      })
  }

  return (
    <div className="fixed top-0 left-0 z-30 w-full h-full pt-20 overflow-auto bg-white bg-gray-200 bg-opacity-50 border rounded-md">
      <div className="flex flex-col items-center justify-center w-4/5 p-12 m-auto bg-white rounded-md md:w-3/5 lg:w-2/5">
        <p className="pb-3 text-xl font-bold uppercase">Login</p>
        <div className="flex flex-col justify-center w-full pt-6 space-y-6">
          <button
            type="button"
            className="w-full p-2 px-4 font-bold text-white bg-red-400 rounded-md"
            onClick={() => googleLogin()}
          >
            Login via Google
          </button>
        </div>
      </div>
    </div>
  )
}
