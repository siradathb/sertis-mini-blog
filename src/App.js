import "./App.css"
import { Header } from "./Layout/Header"
import { Card } from "./Components/Card"
import { Login } from "./Authen/Login"
import { useEffect, useState } from "react"
import firebase from "firebase"

function App() {
  const [allCard, setAllCard] = useState([])
  const [newCardModal, setNewCardModal] = useState(false)
  const [newCardData, setNewCardData] = useState({
    author: null,
    header: "react",
    content: null,
    name: null,
    status: "open",
  })
  const firebaseConfig = {
    apiKey: "AIzaSyCi2zrV3ZIwYsPvDGnkUelc8-XF9jRMBxg",
    authDomain: "sertis-mini-blog.firebaseapp.com",
    projectId: "sertis-mini-blog",
    storageBucket: "sertis-mini-blog.appspot.com",
    messagingSenderId: "302546999666",
    appId: "1:302546999666:web:3ecf08e5429e161607a5eb",
  }

  useEffect(() => {
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig)
    } else {
      firebase.app()
    }

    getCard()
  }, [])

  const createCard = () => {
    let result = Object.values(newCardData).some(o => o === null)
    if(result){
      alert("All field in card is required.")
      return
    }
    firebase.firestore()
      .collection("card")
      .add(newCardData)
      .then((docRef) => {
        setNewCardModal(false)
        alert("Document successfully created!")
        setNewCardData({...newCardData,
          header: "",
          content: "",
          name: "",
          status: "",
        })
        getCard()
      })
      .catch((error) => {
        alert("Error adding document: ", error)
      })
  }

  const onChange = (e) => {
    setNewCardData({...newCardData, [e.target.id]: e.target.value})
  }

  const getCard = () => {
    firebase.firestore()
    .collection("card")
    .get()
    .then((querySnapshot) => {
      let list = []
      querySnapshot.forEach((doc) => {
        list.push({
          id: doc.id,
          data: doc.data(),
        })
      })
      setAllCard(list)
    })
  }

  return (
    <div className="h-full p-0 m-0">
      <Header />

      <div className="m-8 space-y-6 card-container">
        {allCard.map((item) => (
          <Card key={item.id} props={item} currentUser={newCardData.author} getCard={() => getCard()}/>
        ))}
      </div>

      {newCardData.author == null ? (
        <Login userInfo={(email) => setNewCardData({...newCardData, author: email})} />
      ) : null}
      
      <div
        className="fixed p-3 transition-opacity bg-blue-400 rounded-full opacity-100 lg:opacity-50 right-5 bottom-5 text-gray-50 hover:opacity-100"
        onClick={() => setNewCardModal(true)}
      >
        <p className="hidden md:flex">New Card</p>
        <p className="flex md:hidden">+</p>
      </div>

      {newCardModal ? (
        <div
          className="fixed bg-white border rounded-md right-5 bottom-5"
          style={{ width: "300px" }}
        >
          <div className="flex flex-col p-6 space-y-3">
            <div className="flex justify-between pb-3">
              <p className="font-bold text-blue-400 uppercase">
                Create new card!
              </p>
              <p
                onClick={() => setNewCardModal(false)}
                style={{ cursor: "pointer" }}
              >
                X
              </p>
            </div>
            <div className="flex flex-col">
              <label className="pb-3">Category</label>
              <select className="w-full p-2 border rounded-md" id="header" value={newCardData.header}  onChange={(e) => onChange(e)}>
                <option id="react" value="react">
                  React
                </option>
                <option id="angular" value="angular">
                  Angular
                </option>
                <option id="vue" value="vue">
                  Vue
                </option>
                <option id="html" value="html">
                  Html
                </option>
                <option id="css" value="css">
                  Css
                </option>
              </select>
            </div>
            <div className="flex flex-col">
              <label className="pb-3">Content</label>
              <textarea className="w-full p-2 border rounded-md" id="content" onChange={(e) => onChange(e)}></textarea>
            </div>
            <div className="flex flex-col">
              <label className="pb-3">Task status</label>
              <select className="w-full p-2 border rounded-md" value={newCardData.status} id="status" onChange={(e) => onChange(e)}>
                <option id="open" value="open">
                  Open
                </option>
                <option id="close" value="close">
                  Closed
                </option>
              </select>
            </div>
            <div className="flex flex-col">
              <label className="pb-3">Name</label>
              <input className="w-full p-2 border rounded-md" id="name" onChange={(e) => onChange(e)}></input>
            </div>
            <div className="flex">
              <label className="pb-3">Author : {newCardData.author}</label>
            </div>
            <div className="flex">
              <button
                type="button"
                className="w-full p-2 px-4 font-bold text-white bg-blue-400 rounded-md"
                onClick={() => createCard()}
              >
                Create
              </button>
            </div>
          </div>
        </div>
      ) : null}
    </div>
  )
}

export default App
