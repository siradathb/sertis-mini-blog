export const Header = () => {
  return (
    <div className="flex justify-between w-full text-sm font-bold uppercase shadow-md whitespace-nowrap bg-gradient-to-r from-green-200 to-blue-500">
      <div className="flex items-center p-3">
        <p className="text-xl transition-transform transform scale-100 md:text-2xl">
          Sertis Mini blog
        </p>
      </div>
    </div>
  )
}
